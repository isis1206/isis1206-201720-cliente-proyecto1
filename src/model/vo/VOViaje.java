package model.vo;

public class VOViaje 
{
	/**
	 * Modela el id del viaje
	 */
	private String idViaje;

	/**
	 * @return the idViaje
	 */
	public String getIdViaje()
	{
		return idViaje;
	}

	/**
	 * @param idViaje the idViaje to set
	 */
	public void setIdViaje(String idViaje)
	{
		this.idViaje = idViaje;
	}
	
	

}
